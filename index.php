<?php
include_once 'classes/DataHandler.php';
include_once 'classes/globaldefines.php';
include_once 'classes/Vehicle.php';
include_once 'classes/Worldspace.php';
include_once 'classes/GPSLocation.php';
include_once 'classes/Printer.php';
include_once 'classes/VehicleFinder.php';

$currentGPS = isset ( $_POST ["GPS"] ) ? $_POST ["GPS"] : "";
$currentGPSX = isset ( $_POST ["x"] ) ? $_POST ["x"] : "";
$currentGPSY = isset ( $_POST ["y"] ) ? $_POST ["y"] : "";
$radius = isset ( $_POST ["radius"] ) ? $_POST ["radius"] : "10";
$type = isset ( $_POST ["type"] ) ? $_POST ["type"] : "-1";
$toJson = isset ( $_GET ["json"] ) ? $_GET ["json"] : null;
function isSelected($var1, $var2) {
	if ($var1 == $var2)
		return IS_SELECTED;
	else
		return "";
}

$dataHandler = new DataHandler ();
$printer = new Printer ();

$vehicles = $dataHandler->requestRemoteData ( BASE_SQL_URL );

$errors = array ();

if (! empty ( $currentGPS )) {
	
	if (strlen ( str_replace ( ' ', '', $currentGPS ) ) != 6) {
		array_push ( $errors, ERROR_WRONG_GPS_COORDS_LEN );
	}
	
	$currentGPSX = substr ( $currentGPS, 0, 3 );
	$currentGPSY = substr ( $currentGPS, 3 );
}

$vehicleFinder = new VehicleFinder ( $vehicles );
$found = $vehicleFinder->find ( $type, $currentGPSX, $currentGPSY, $radius );

if (! empty ( $toJson )) {
	$printer->printVehicleDataJSON ( $found );
} else {
	?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DayZ Vehicle Spawn Finder</title>
<link href="css/style.css" type="text/css" rel="stylesheet">
</head>
<body>
	<div>
		<form method="post">
			<input name="action" type="hidden" value="find" />
			<fieldset>
				<legend>DayZ:Mod GPS Vehicle Finder</legend>
				<div class="inputContainer">
					<div class="leftFloat">
						<label for="GPS_INPUT">GPS COORDS</label> <input id="GPS_INPUT"
							name="GPS" type="text" value="<?php echo($currentGPS)?>"
							maxlength="7" />
					</div>
					<div class="leftFloat">
						<label for="RADIUS">RADIUS</label> <select id="RADIUS"
							name="radius">
							<option value="1" <?php echo(isSelected(1,$radius))?>>1</option>
							<option value="5" <?php echo(isSelected(5,$radius))?>>5</option>
							<option value="10" <?php echo(isSelected(10,$radius))?>>10</option>
							<option value="15" <?php echo(isSelected(15,$radius))?>>15</option>
						</select>
					</div>
					<div class="leftFloat">
						<label for="TYPE">VEHICLE</label> <select id="TYPE" name="type">
							<option value="-1" <?php echo(isSelected(-1, $type))?>>All</option>
							<option value="0" <?php echo(isSelected(0, $type))?>>ATV</option>
							<option value="1" <?php echo(isSelected(1, $type))?>>Motorcycle</option>
							<option value="2" <?php echo(isSelected(2, $type))?>>Huey</option>
							<option value="3" <?php echo(isSelected(3, $type))?>>Little Bird</option>
							<option value="4" <?php echo(isSelected(4, $type))?>>Mi-17</option>
							<option value="5" <?php echo(isSelected(5, $type))?>>AN-2</option>
							<option value="6" <?php echo(isSelected(6, $type))?>>Bike</option>
							<option value="7" <?php echo(isSelected(7, $type))?>>Military Car</option>
							<option value="10" <?php echo(isSelected(10, $type))?>>Civilian
								Car</option>
							<option value="11" <?php echo(isSelected(11, $type))?>>Bus</option>
							<option value="12" <?php echo(isSelected(12, $type))?>>Tractor</option>
							<option value="13" <?php echo(isSelected(13, $type))?>>Truck</option>
							<option value="14" <?php echo(isSelected(14, $type))?>>Boat</option>
						</select>
					</div>
					<div class="leftFloat">
						<label for="submit">&nbsp;</label> <input id="submit"
							type="submit" />
					</div>
				</div>
			</fieldset>
		</form>

<?php 
$printer->printVehiclesGPSData($found);
}
?>
</div>
</body>
</html>





