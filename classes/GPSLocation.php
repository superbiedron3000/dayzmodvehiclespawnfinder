<?php

class GPSLocation{
	    
	public $xCoord;
	public $yCoord;
	private $worldSpaceX;
	private $worldSpaceY;
	private $SINGLE_GRID_DIMENSION;
	
	
	function __construct($x, $y){
		$this->SINGLE_GRID_DIMENSION = $this->calculateSingleGrid();
		$this->xCoord = $this->getArmaX($x);
		$this->yCoord = $this->getArmaY($y);
		$this->worldSpaceX = $x;
		$this->worldSpaceY = $y;
	}
	
	private function getArmaX($x){
		$xCoord = $x / $this->SINGLE_GRID_DIMENSION;
		return $xCoord;
	}
	
	private function getArmaY($y){
		$yCoord = $y / $this->SINGLE_GRID_DIMENSION;
		$yCoord = MAX_GRIDS - $yCoord;
		return  $yCoord;
	}
	
	public function getWorldX(){
		return $this->worldSpaceX;
	}
	
	public function getWorldY(){
		return $this->worldSpaceY;
	}
	
	private function calculateSingleGrid() {
		return (float)MAP_DIMENSION / (float)MAX_GRIDS;
	}
	
	public function getX() {
		return $this->xCoord;
	}
	
	public function getY() {
		return $this->yCoord;
	}
}

?>