# README #

### What is this application for? ###

This is a proxy for looking up vanilla vehicle spawns in DayZ:Mod for Arma2.

It's based on a GitHub file lookup (https://raw.githubusercontent.com/DayZMod/DayZ/Development/SQL/1.8.8/vehicle_locations.sql) for base coordinates.

### How do I run it ? ###
Just deploy the files to a PHP enabled webserver.

### How to use it? ###

Input your ingame GPS coordinates, setup the radius to look in, and select the vehicle type to show. The app will show you what vehicle spawns are in the designated area.

### Contributors ###

* Michał Bierwiaczonek (http://rzeczywmiejscach.pl) - PHP development
* Łukasz Górny (https://lukasgorny.pro/) - consultations & testing

### Version ###
* beta (0.4)