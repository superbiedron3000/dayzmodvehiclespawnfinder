<?php
class Printer{
	function printVehiclesGPSData($vehicles) {
		echo ("<ul>");
		foreach ($vehicles as $vehicle) {
			echo(sprintf("<li>%s</li>" ,$vehicle->printSelf()));
		}
		echo ("</ul>");
	}
	
	function printVehicleDataJSON($vehicles) {
		echo json_encode($vehicles);
	}
}