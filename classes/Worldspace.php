<?php
class Worldspace {
	private $azimuth;
	private $x;
	private $y;
	private $z;
	
	function __construct($azimuth, $x, $y, $z) {
		$this->azimuth = $azimuth;
		$this->x = $x;
		$this->y = $y;
		$this->z = $z;
	}
	
	function getX(){
		return $this->x;
	}
	
	function getY(){
		return $this->y;
	}
}