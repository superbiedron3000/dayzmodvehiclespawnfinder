<?php

class Vehicle {
	public $type;
	public $gpsLocation;

	function __construct($type, $worldSpaceCoords){
		$this->type = $type;
		$this->gpsLocation = new GPSLocation($worldSpaceCoords->getX(), $worldSpaceCoords->getY());
	}

	function getVehicleType(){
		return $this->type;
	}

	function printSelf(){
		$main =	sprintf("[%s] [%s %s]", $this->getVehicleTypeName(), $this->getGPSX(), $this->getGPSY());
		$debug = sprintf("DEBUG [%s, %s] | ",$this->gpsLocation->getWorldX(),$this->gpsLocation->getWorldX());
		return DEBUG ? $debug . $main : $main;
	}

	function toJSON(){
		json_encode($this);
	}

	function getX(){
		return $this->gpsLocation->getX();
	}

	function getY(){
		return $this->gpsLocation->getY();
	}

	function getGPSX(){
		$x = (int)$this->gpsLocation->getX();
		if (strlen($x) < 3){
			$x = "0".$x;
		}
		return $x;
	}
	function getGPSY() {
		$y = (int) $this->gpsLocation->getY();
		if (strlen($y) < 3){
			$y = "0".$y;
		}
		return $y;
	}

	function getVehicleTypeName(){

		/*
		 * 0 ATV\r\n
		 * 1 Motorcycle\r\n
		 * 2 Huey\r\n
		 * 3 Little bird\r\n
		 * 4 Mi-17\r\n
		 * 5 AN-2\r\n
		 * 6 Bike\r\n
		 * 7 Military car (Landrover, HMMWV, Camo UAZs, Armed pickups)\r\n
		 * 10 Civilian car (including SUV)\r\n
		 * 11 Bus\r\n
		 * 12 Tractor\r\n
		 * 13 Truck\r\n
		 * 14 Boat
		 *
		 */

		switch ($this->type){
			case 0:
				return "ATV";
			case 1:
				return "Motorcycle";
			case 2:
				return "HUEY";
			case 3:
				return "Little Bird";
			case 4:
				return "Mi-17";
			case 5:
				return "AN-2";
			case 6:
				return "Bike";
			case 7:
				return "Military Car";
			case 10:
				return "Civilian Car";
			case 11:
				return "Bus";
			case 12:
				return "Tractor";
			case 13:
				return "Truck";
			case 14:
				return "Boat";
			default:
				return "";
		}
	}

}
