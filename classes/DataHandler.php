<?php

class DataHandler {
	
	private $dataResult = "";
	
	function requestRemoteData($URL){
		$raw = file_get_contents($URL);
		$vehicleArray = $this->processData($raw);
		
		return $vehicleArray;
	}
	
	private function processData($rawData){
		$regexPattern = "/(?<!(\/\*.)|(\/\*))\([0-9]*, *'\[.*]'\)/"; //(0,'[126,[6556.34,5621.66,0]]')
		$charsToRepalce = array ("(", "'", "[", "]", ")");
		preg_match_all($regexPattern, $rawData, $matches);

		$vehicleArray = array();
		
		foreach ($matches[0] as $value){
			$strvalue = str_replace($charsToRepalce, "", $value); //0,126,6556.34,5621.66,0
			$keywords = explode(",", $strvalue);
			array_push($vehicleArray, new Vehicle($keywords[0], new Worldspace($keywords[1], $keywords[2], $keywords[3], $keywords[4])));
		}
		
		return $vehicleArray;
	}
	
	
	
}