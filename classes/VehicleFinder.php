<?php
class VehicleFinder {

	private $vehicleSet;

	function __construct($vehicleSet){
		$this->vehicleSet = $vehicleSet;
	}

	function find($type=null, $x=null, $y=null, $radius=null) {
		$results = array();
		if (!isset($radius) || empty($radius)){
			$radius = 5;
		}
		if ($type == -1){
			unset($type);
		}
		if (isset($type)){
				
			foreach ($this->vehicleSet as $vehicle) {
				if ($vehicle->getVehicleType() == $type ){
					array_push($results, $vehicle);
				}
			}
		} else {
			$results = $this->vehicleSet;
		}
		//wstepnie wysortowane typy lub ca�o�� wynik�w
		if (isset($x) && (!empty($x)) && isset($y) && (!empty($y))){
			$typedResults = array();
			foreach ($results as $vehicle) {

				$xCoord = $vehicle->getX();
				$yCoord = $vehicle->getY();

				if (($x-$radius < $xCoord) && ( $xCoord < $x + $radius) && ($y-$radius < $yCoord ) && ( $yCoord < $y + $radius)){
					array_push($typedResults, $vehicle);
				}
			}
			$results = $typedResults;
		}
		return $results;
	}
}