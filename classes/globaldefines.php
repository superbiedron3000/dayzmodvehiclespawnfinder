<?php
define('MAX_GRIDS' ,"153.0");
define('MAP_DIMENSION' ,"15360.0");
define('BASE_SQL_URL','https://raw.githubusercontent.com/DayZMod/DayZ/Development/SQL/1.8.8/vehicle_locations.sql');
define('ERROR_WRONG_GPS_COORDS_LEN','GPS coorginates must be 6 digits long');
define('IS_SELECTED','selected="selected"');
define('DEBUG', false);